"""Generate Office365 & Limacharlie Edr log."""
from faker import Faker
from datetime import datetime
from dateutil import relativedelta
from pytz import timezone
import json
import argparse
from faker.providers import file
import random

MALICIOUS_EXTENSIONS = [
    "com",
    "exe",
    "bat",
    "cmd",
    "cpl",
    "jar",
    "js",
    "msi",
    "rar",
    "reg",
]
ACTION_TYPE = "10"
SRC_SERVICE = "smtp-relay"
SRC_SELECTOR = "gmail-for-work"
DES_SERVICE = "smtp-outbound"
DES_SELECTOR = "relay"
MESSAGE_SET = [
    {"type": "22"},
    {"type": "39"},
    {"type": "8"},
    {"type": "29"},
    {"type": "37"},
    {"type": "27"},
    {"type": "10"},
    {"type": "16"},
]
SMTP_REPLY_CODE = "250"


def get_timestamp_within_6_month(faker_obj):
    """Get random unix timestamp within last 6 months

    Args:
        faker_obj ([faker]): Instance of faker library.

    Returns:
        [int]: Time stamp in milliseconds
    """
    n = datetime.now()
    d = n - relativedelta.relativedelta(months=6)
    timestamp_usec = (
        faker_obj.date_time_between(
            start_date=d,
            end_date="now",
        )
        .replace(tzinfo=timezone("UTC"))
        .timestamp()
        * 1000
    )
    return timestamp_usec


def generate_gmail_log(faker_obj, is_malicious=True):
    """Generate gmail log

    Args:
        faker_obj ([faker]): Instance of faker library.
        is_malicious (bool, optional): Set true to generate log with malicious attachment file
        Defaults to False.

    Returns:
        [dict]: gmail log
    """
    gmail_log = {
        "event_info": {
            "timestamp_usec": str(get_timestamp_within_6_month(faker_obj) * 1000),
            "elapsed_time_usec": str(faker_obj.random_int(1000000, 2000000)),
            "success": True,
        },
        "message_info": {
            "action_type": ACTION_TYPE,
            "rfc2822_message_id": f"<{faker_obj.word()}.{faker_obj.word()}@localhost>",
            "subject": faker_obj.sentence(),
            "payload_size": faker_obj.random_int(100000, 300000),
            "smtp_reply_code": SMTP_REPLY_CODE,
            "source": {
                "address": faker_obj.email(),
                "service": SRC_SERVICE,
                "selector": SRC_SELECTOR,
            },
            "destination": [
                {
                    "address": faker_obj.email(),
                    "service": DES_SERVICE,
                    "selector": DES_SELECTOR,
                }
                for _ in range(faker_obj.random_int(2, 10))
            ],
            "connection_info": {
                "client_ip": faker_obj.ipv4(),
                "smtp_reply_code": SMTP_REPLY_CODE,
            },
            "message_set": MESSAGE_SET,
            "attachment": [{}]
            + [
                {
                    "sha256": faker_obj.sha256(),
                    "file_extension_type": (
                        lambda: "".join(faker_obj.words(1, MALICIOUS_EXTENSIONS, True))
                        if is_malicious
                        else faker_obj.file_extension()
                    )(),
                }
                for _ in range(faker_obj.random_int(1, 9))
            ],
        },
    }
    return gmail_log


def generate_limacharlie_edr_log(faker, mail_log):
    """Generate Limacharlie Edr log.

    Args:
        faker (faker): instance of faker liabrary
        mail_log (dict): office365 log data

    Returns:
        dict: limacharlie edr log data
    """
    limacharlie_edr_log = {}

    # routing data
    routing = {}

    # OrganizationId
    routing["oid"] = faker.uuid4()

    # Installer ID
    routing["iid"] = faker.uuid4()

    # Sensor ID
    routing["sid"] = faker.uuid4()

    # Architecture
    routing["arch"] = faker.random_int(1, 8)

    # Platform
    routing["plat"] = int(str(faker.random_int(1, 7)) + "0000000", 16)

    # Hostname
    routing["hostname"] = faker.word()
    # Internal IP address
    routing["int_ip"] = mail_log["message_info"]["connection_info"]["client_ip"]

    # External IP address
    routing["exp_ip"] = faker.ipv4()

    # Module ID
    routing["moduleid"] = 2

    # Event type
    routing["event_type"] = "NEW_PROCESS"

    # Event Time
    # end_date = datetime.strptime(
    #     mail_log["CreationTime"], "%Y-%m-%dT%H:%M:%S"
    # ) + relativedelta.relativedelta(hours=48)
    # current_time = datetime.now()
    # routing["event_time"] = (
    #     faker.date_time_between(
    #         start_date=datetime.strptime(mail_log["CreationTime"], "%Y-%m-%dT%H:%M:%S"),
    #         end_date=(lambda: end_date if end_date < current_time else current_time)(),
    #     )
    #     .replace(tzinfo=timezone("UTC"))
    #     .timestamp()
    #     * 1000
    # )

    current_time = datetime.now().timestamp() * 1000
    routing["event_time"] = min(
        float(mail_log["event_info"]["timestamp_usec"]) // 1000 + 1000, current_time
    )

    # Event ID
    routing["event_id"] = faker.uuid4()

    # Tags
    routing["tags"] = [faker.word()]

    routing["this"] = faker.md5()
    routing["parent"] = faker.md5()

    limacharlie_edr_log["routing"] = routing

    # Events
    event = {}

    event["PARENT_PROCESS_ID"] = faker.random_int()

    # File Path
    file1 = f"C:\\User\\{faker.word()}.{mail_log['message_info']['attachment'][1]['file_extension_type']}"
    file2 = f"C:\\Users\\{faker.word()}\\AppData\\Local\\Temp\\{faker.word()}.{mail_log['message_info']['attachment'][1]['file_extension_type']}"
    file3 = f"D:\\{faker.word()}\\{faker.word()}.{mail_log['message_info']['attachment'][1]['file_extension_type']}"

    file_path = [file1, file2]

    file_path = faker.words(1, file_path, True)[0]
    event["FILE_PATH"] = (
        lambda: file_path
        if mail_log["message_info"]["attachment"][1]["file_extension_type"]
        in MALICIOUS_EXTENSIONS
        else file3
    )()

    # Command line
    event["COMMAND_LINE"] = f'"{file_path}"'

    # Base Address
    event["BASE_ADDRESS"] = faker.random_int(1000, 10000)

    # Process ID
    event["PROCESS_ID"] = faker.random_int(10, 10000)

    # Threads
    event["THREADS"] = faker.random_int(1, 20)

    # Memory Usage
    event["MEMORY_USAGE"] = faker.random_int(100, 10000000)

    # User name
    event["USER_NAME"] = faker.word()

    limacharlie_edr_log["event"] = event

    # Parent
    parent = {}

    parent["PARENT_PROCESS_ID"] = faker.random_int(10, 10000)

    # File path
    parent["FILE_PATH"] = event["FILE_PATH"]
    parent["COMMAND_LINE"] = event["COMMAND_LINE"]

    # Memory Usage
    parent["MEMORY_USAGE"] = faker.random_int(100, 10000000)

    parent["USER_NAME"] = event["USER_NAME"]

    # Process ID
    parent["PROCESS_ID"] = faker.random_int(10, 10000)

    # timestamp
    parent["TIMESTAMP"] = routing["event_time"]

    # This Atom
    parent["THIS_ATOM"] = faker.md5()

    # Parent Atom
    parent["PARENT_ATOM"] = faker.md5()
    parent["FILE_IS_SIGNED"] = 0

    # random hash
    parent["HASH"] = faker.sha256()

    routing["parent"] = parent

    routing["FILE_IS_SIGNED"] = 0
    routing["HASH"] = mail_log["message_info"]["attachment"][1]["sha256"]

    return limacharlie_edr_log


def generate_office365_log(faker, is_malicious=True):
    """Generate Office365 mail log.

    Args:
        faker (faker): instance of faker liabrary

    Returns:
        dict: office365 mail log
    """
    mail_log = {}

    n = datetime.now()
    d = n - relativedelta.relativedelta(months=6)

    # Date-Time between today and last 6 months
    creation_time = faker.date_time_between(start_date=d, end_date="now")
    creation_time2 = "T".join(str(creation_time).split())
    print(creation_time2)
    mail_log["CreationTime"] = str(creation_time2)

    # Generate Ipv4 address
    mail_log["ClientIP"] = faker.ipv4()
    mail_log["ExchangeDetails"] = {}
    # sender mail id
    mail_log["ExchangeDetails"]["From"] = faker.company_email()

    mail_log["ExchangeDetails"]["InternetMessageId"] = faker.uuid4()

    # message time
    message_time = creation_time - relativedelta.relativedelta(minutes=10)
    mail_log["ExchangeDetails"]["MessageTime"] = str(
        "T".join(str(message_time).split())
    )

    mail_log["ExchangeDetails"]["NetworkMessageId"] = faker.uuid4()

    attachment_data = []

    for _ in range(0, faker.random_int(1, 5)):
        file_name = faker.file_name().split(".")
        attachment_data.append(
            {
                "FileName": file_name[0],
                "FileType": (
                    lambda: "".join(faker.words(1, MALICIOUS_EXTENSIONS, True))
                    if is_malicious is True
                    else file_name[1]
                )(),
                "SHA256": faker.sha256(),
            }
        )

    mail_log["ExchangeDetails"]["AttachmentData"] = attachment_data
    # Recipients
    recipients = []
    for _ in range(0, faker.random_int(1, 5)):
        recipients.append(faker.email())
    mail_log["ExchangeDetails"]["Recipients"] = recipients

    # Subject of mail
    mail_log["ExchangeDetails"]["Subject"] = faker.sentence()

    mail_log["Id"] = faker.uuid4()

    object_id = faker.email()
    mail_log["ObjectId"] = object_id
    mail_log["Operation"] = "SupervisoryReviewOLAudit"
    mail_log["OrganizationId"] = faker.uuid4()
    mail_log["RecordType"] = 68
    mail_log["UserKey"] = "".join([str(faker.random_int(0, 9)) for _ in range(0, 10)])
    mail_log["ResultStatus"] = "Threat Model {}Positive Score:{}".format(
        "".join(faker.words(1, ["False ", ""], True)), faker.random_int(42, 100)
    )
    mail_log["UserId"] = object_id
    mail_log["UserType"] = 0
    mail_log["Version"] = 1
    mail_log["Workload"] = "Exchange"
    mail_log["SecurityComplianceCenterEventType"] = 0
    return mail_log


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-n", "--number", type=int, help="Number of log entries to generate", default=1
    )

    args = parser.parse_args()
    faker_obj = Faker()
    faker_obj.add_provider(file)
    if args.number < 1:
        print("Invalid number of log entries specified")
    else:
        non_malicious_log_count = int(args.number * 0.4)
        with open("gmail.log", "w") as gmail, open("limacharlie_edr.log", "w") as lima:
            for i in range(0, args.number):
                gmail_log = None
                if non_malicious_log_count > i:
                    gmail_log = generate_gmail_log(faker_obj, False)
                else:
                    gmail_log = generate_gmail_log(faker_obj)
                print(gmail_log)
                limacharlie_edr_log = generate_limacharlie_edr_log(faker_obj, gmail_log)
                print(limacharlie_edr_log["routing"]["int_ip"])
                gmail.write(json.dumps(json.dumps(gmail_log, separators=(",", ":"))))
                lima.write(
                    json.dumps(json.dumps(limacharlie_edr_log, separators=(",", ":")))
                )
                if i != (args.number - 1):
                    gmail.write(",")
        # with open("office365.log", "w") as f, open("limacharlie_edr.log", "w") as lima:
        #     for i in range(0, args.number):
        #         office365_log = None
        #         if non_malicious_log_count > i:
        #             office365_log = generate_office365_log(faker, False)
        #         else:
        #             office365_log = generate_office365_log(faker)
        #         print(office365_log)
        #         limacharlie_edr_log = generate_limacharlie_edr_log(faker, office365_log)
        #         print(limacharlie_edr_log["routing"]["int_ip"])
        #         f.write(json.dumps(json.dumps(office365_log, separators=(",", ":"))))
        #         lima.write(
        #             json.dumps(json.dumps(limacharlie_edr_log, separators=(",", ":")))
        #         )
        #         if i != (args.number - 1):
        #             f.write(",")
