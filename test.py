"""Unit testing for functions in log_generator"""
from datetime import datetime
from dateutil import relativedelta
from log_generator import (
    generate_office365_log,
    generate_limacharlie_edr_log,
    generate_gmail_log,
)
from faker import Faker
import pytest

EDR_FIELDS = [
    "routing",
    "oid",
    "iid",
    "sid",
    "arch",
    "plat",
    "hostname",
    "int_ip",
    "exp_ip",
    "moduleid",
    "event_type",
    "event_time",
    "event_id",
    "tags",
    "this",
    "parent",
    "PARENT_PROCESS_ID",
    "FILE_PATH",
    "COMMAND_LINE",
    "MEMORY_USAGE",
    "USER_NAME",
    "PROCESS_ID",
    "TIMESTAMP",
    "THIS_ATOM",
    "PARENT_ATOM",
    "FILE_IS_SIGNED",
    "HASH",
    "FILE_IS_SIGNED",
    "HASH",
    "event",
    "PARENT_PROCESS_ID",
    "FILE_PATH",
    "COMMAND_LINE",
    "BASE_ADDRESS",
    "PROCESS_ID",
    "THREADS",
    "MEMORY_USAGE",
    "USER_NAME",
]
O365_FIELDS = [
    "CreationTime",
    "ClientIP",
    "ExchangeDetails",
    "AttachmentData",
    "From",
    "InternetMessageId",
    "MessageTime",
    "NetworkMessageId",
    "Recipients",
    "Subject",
    "Id",
    "ObjectId",
    "Operation",
    "OrganizationId",
    "RecordType",
    "UserKey",
    "ResultStatus",
    "UserId",
    "UserType",
    "Version",
    "Workload",
    "SecurityComplianceCenterEventType",
]

GMAIL_FIELDS = [
    "service",
    "action_type",
    "timestamp_usec",
    "message_set",
    "smtp_reply_code",
    "selector",
    "event_info",
    "attachment",
    "file_extension_type",
    "client_ip",
    "source",
    "destination",
    "elapsed_time_usec",
    "success",
    "address",
    "sha256",
    "message_info",
    "type",
    "connection_info",
    "rfc2822_message_id",
    "payload_size",
    "subject",
]


@pytest.fixture()
def office365_data():
    """Generate office365 data.

    Returns:
        dict: office365 data
    """
    faker_obj = Faker()
    office365_data = generate_office365_log(faker_obj)

    return office365_data


@pytest.fixture()
def gmail_data():
    faker_obj = Faker()
    return generate_gmail_log(faker_obj)


def store_keys(d: dict, arr: list):
    """Stores all keys of a (nested) dictionary
    in given list

    Args:
        d (dict): [simple or nested dictionary]
        arr (list): [list to store all keys]
    """
    for k in list(d.keys()):
        # print(k)
        arr.append(k)
        if type(d[k]) is dict:
            store_keys(d[k], arr)
        if type(d[k]) is list:
            for e in d[k]:
                if type(e) is dict:
                    store_keys(e, arr)


@pytest.fixture()
def lm_edr_data(gmail_data):
    """Fixture to generate limacharlie data

    Args:
        office365_data ([dict]): office 365 data

    Returns:
        [dict]: [limacharlie edr data]
    """
    faker_obj = Faker()
    lm_edr_data = generate_limacharlie_edr_log(faker_obj, gmail_data)
    return lm_edr_data


def test_gmail_date(gmail_data):
    now = datetime.now()
    minimum_time = now - relativedelta.relativedelta(months=6)
    event_time = datetime.fromtimestamp(
        float(gmail_data["event_info"]["timestamp_usec"]) // 1000000
    )
    assert event_time >= minimum_time


def test_gmail_fields(gmail_data):
    fields_to_check = []
    store_keys(gmail_data, fields_to_check)
    fields_to_check = list(set(fields_to_check))
    fields_to_check.sort()
    GMAIL_FIELDS.sort()
    assert fields_to_check == GMAIL_FIELDS


def test_lm_edr_date(lm_edr_data):
    """Testcase method for checking office365 log creation time is last 6 month.

    Args:
        office365_data ([dict]): [office365 log data]
    """
    now = datetime.now()
    minimum_time = now - relativedelta.relativedelta(months=6)

    event_time = datetime.fromtimestamp(lm_edr_data["routing"]["event_time"] // 1000)
    assert event_time >= minimum_time


# def test_office365_mail_date(office365_data):
#     """Testcase method for checking office365 log creation time is last 6 month.

#     Args:
#         office365_data ([dict]): [office365 log data]
#     """
#     now = datetime.now()
#     minimum_time = now - relativedelta.relativedelta(months=6)

#     creation_time = datetime.strptime(
#         office365_data["CreationTime"], "%Y-%m-%dT%H:%M:%S"
#     )
#     assert creation_time >= minimum_time


# def test_office365_mail_fields(office365_data):
#     """Testcase method for checking all the fields
#     in office 365 log exists in proper structure.

#     Args:
#         office365_data ([dict]): [office365 log data]
#     """
#     attachment_data = ["FileName", "FileType", "SHA256"]

#     fields_to_check = []
#     fields_to_check += list(office365_data.keys())
#     assert "ExchangeDetails" in fields_to_check
#     assert type(office365_data["ExchangeDetails"]) is dict
#     fields_to_check += list(office365_data["ExchangeDetails"].keys())
#     assert "AttachmentData" in office365_data["ExchangeDetails"]
#     assert type(office365_data["ExchangeDetails"]["AttachmentData"]) is list
#     for atch in office365_data["ExchangeDetails"]["AttachmentData"]:
#         assert type(atch) is dict
#         assert attachment_data == list(atch.keys())
#     # print(fields_to_check)
#     O365_FIELDS.sort()
#     fields_to_check.sort()
#     assert fields_to_check == O365_FIELDS


def test_lm_edr_fields(lm_edr_data):
    """Testcase to Check if all fields are
    present in limachrlie log

    Args:
        lm_edr_data ([dict]): [limachrlie log]
    """
    fields_to_check = []
    store_keys(lm_edr_data, fields_to_check)
    fields_to_check.sort()
    EDR_FIELDS.sort()
    assert fields_to_check == EDR_FIELDS
